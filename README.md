# desafio-eiti  
Desafio para vaga de Desenvolvedor Java Pleno na Eiti  
  
** Aplicação hospedada no Heroku  
Link: https://desafio-eiti.herokuapp.com/usersList  
  
**Tecnologias utilizadas  
Spring  
FlyWay  
  
**Metodologias  
MVC  
Rest  
  
**Deploy  
Heroku  
  
**Estratégia  
Primeiramente foi analisada a descrição das atividades e tecnologias necessárias  
As exceções foram referentes banco de dados, utilizei o Postgresql, que tenho mais conhecimento  
  
**Auto-avaliação  
Fiquei satisfeito com todas as solicitações e soluções de entregues.  
Talvez não seja necessário para o projeto, mas tenho costume de trabalhar com o back-end e front-end separados.  
Atualmente por exemplo, tenho trabalho com Spring e Angular 4.  
Acredito que por falta de atenção minha, porém não ficou claro se a atividade era somente de listagem ou também de cadastro; então foi realizado um CRUD.  
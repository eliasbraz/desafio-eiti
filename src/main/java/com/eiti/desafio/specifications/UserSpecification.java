package com.eiti.desafio.specifications;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.eiti.desafio.entities.User;

public class UserSpecification implements Specification<User> {
	
	private static final long serialVersionUID = 1L;

	private User user;

	@Override
	public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
		
		if (!user.getUsername().equals("")) {
			return builder.like(root.get("username"), "%" + user.getUsername() + "%");
		}
		
		if (!user.getName().equals("")) {
			return builder.like(root.get("name"), "%" + user.getName() + "%");
		}
		
		if (!user.getEmail().equals("")) {
			return builder.like(root.get("email"), "%" + user.getEmail() + "%");
		}
		
		return null;
	}

}

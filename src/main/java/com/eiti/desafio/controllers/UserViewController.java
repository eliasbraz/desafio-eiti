package com.eiti.desafio.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.eiti.desafio.entities.User;
import com.eiti.desafio.repositories.UserRepository;

@Controller
public class UserViewController {

	@Autowired
	private UserRepository repository;

	@RequestMapping(value = "/usersList", method = RequestMethod.GET)
	public String usersList(Model model, User user) {
		model.addAttribute("users", repository.findAll(verificaFiltros(user)));

		return "usersList";
	}

	@RequestMapping(value = "/createUser", method = RequestMethod.GET)
	public String createViewUser(Model model, @PathParam(value = "id") Integer id) {
		User user;

		if (id == null) {
			user = new User();
		} else {
			user = repository.getOne(id);

			if (user == null)
				user = new User();
		}

		model.addAttribute("user", user);

		return "createUser";
	}

	@RequestMapping(value = "/saveUser", method = RequestMethod.POST)
	public String saveUser(@ModelAttribute("user") User user, BindingResult results, ModelMap model) {
		repository.save(user);

		return "redirect:usersList";
	}

	@RequestMapping(value = "/deleteUser", method = RequestMethod.GET)
	public String deleteUser(@PathParam(value = "id") Integer id) {
		repository.deleteById(id);

		return "redirect:usersList";
	}

	@SuppressWarnings("serial")
	private Specification<User> verificaFiltros(User user) {
		return new Specification<User>() {

			@Override
			public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
				List<Predicate> predicates = new ArrayList<>();

				if (user.getUsername() != null && !user.getUsername().isEmpty()) {
					predicates.add(builder.equal(root.get("username"), user.getUsername()));
				}

				if (user.getName() != null && !user.getName().isEmpty()) {
					predicates.add(builder.like(root.get("name"), "%" + user.getName() + "%"));
				}

				if (user.getEmail() != null && !user.getEmail().isEmpty()) {
					predicates.add(builder.equal(root.get("email"), user.getEmail()));
				}

				return query.where(builder.and(predicates.toArray(new Predicate[predicates.size()])))
						.getGroupRestriction();
			}

		};
	}
}

package com.eiti.desafio.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eiti.desafio.entities.User;
import com.eiti.desafio.repositories.UserRepository;

@RestController
@RequestMapping(value = "/users")
public class UserController {

	@Autowired
	private UserRepository repository;

	@GetMapping
	public List<User> getUsers(User user) {
		return repository.findAll(verificaFiltros(user));
	}

	@PostMapping
	public User postUser(@RequestBody @Valid User user) {
		return repository.save(user);
	}

	@SuppressWarnings("serial")
	private Specification<User> verificaFiltros(User user) {
		return new Specification<User>() {

			@Override
			public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
				List<Predicate> predicates = new ArrayList<>();

				if (user.getUsername() != null) {
					predicates.add(builder.equal(root.get("username"), user.getUsername()));
				}

				if (user.getName() != null) {
					predicates.add(builder.like(root.get("name"), "%" + user.getName() + "%"));
				}

				if (user.getEmail() != null) {
					predicates.add(builder.equal(root.get("email"), user.getEmail()));
				}

				return query.where(builder.and(predicates.toArray(new Predicate[predicates.size()])))
						.getGroupRestriction();
			}

		};
	}
}

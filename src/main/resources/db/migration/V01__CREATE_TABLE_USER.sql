CREATE TABLE "user" (
	id SERIAL NOT NULL,
	email VARCHAR(255) NOT NULL, 
	is_enabled BOOLEAN NOT NULL, 
	name VARCHAR(255) NOT NULL, 
	password VARCHAR(255) NOT NULL, 
	phone VARCHAR(255) NOT NULL, 
	register_date TIMESTAMP NOT NULL, 
	surname VARCHAR(255) NOT NULL, 
	username VARCHAR(255) NOT NULL, 
	
	PRIMARY KEY (id)
);

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>User List</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css"
	integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="/css/style.css">
</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<div class="container">
			<a class="navbar-brand" href="#">Desafio Eiti</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarNav" aria-controls="navbarNav"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<li class="nav-item"><a class="nav-link" href="/usersList">List</a></li>
					<li class="nav-item"><a class="nav-link" href="/createUser">Create
							User</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<br>
	<div class="container container-fluid">
		<div class="row">
			<div class="col-md-12">
				<h1>User List</h1>
				<form>
					<div class="row">
						<div class="col-md-2 form-group">
							<label for="username">Username</label> <input type="text"
								class="form-control" id="username" name="username"
								aria-describedby="username" placeholder="Enter Username">
						</div>
						<div class="col-md-3 form-group">
							<label for="name">Name</label> <input type="text"
								class="form-control" id="name" name="name"
								aria-describedby="name" placeholder="Enter Name">
						</div>
						<div class="col-md-3 form-group">
							<label for="email">Email</label> <input type="email"
								class="form-control" id="email" name="email"
								aria-describedby="email" placeholder="Enter Email">
						</div>
						<div class="col-md-2 form-group">
							<label>&nbsp;</label>
							<button type="submit" class="btn btn-success form-control">Search</button>
						</div>
						<div class="col-md-2 form-group">
							<label>&nbsp;</label> <a class="btn btn-secondary form-control"
								href="/usersList" role="button">Clear</a>
						</div>
					</div>
				</form>

				<div class="table-responsive">
					<table class="table table-hover">
						<thead class="thead-light">
							<tr>
								<th>#</th>
								<th>Username</th>
								<th>Is Enabled</th>
								<th>Register Date</th>
								<th>Name</th>
								<th>Surname</th>
								<th>Email</th>
								<th>Phone</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${users}" var="user">
								<tr class="clickable-row" data-href="/createUser?id=${user.id}"
									style="cursor: pointer;">
									<td scope="row">${user.id}</td>
									<td>${user.username}</td>
									<td>${user.isEnabled}</td>
									<td>${user.registerDate}</td>
									<td>${user.name}</td>
									<td>${user.surname}</td>
									<td>${user.email}</td>
									<td>${user.phone}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12" style="text-align: right;">
				<a class="btn btn-primary" href="/createUser" role="button">Create
					User</a>
			</div>
		</div>
	</div>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
		integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js"
		integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em"
		crossorigin="anonymous"></script>

	<script>
		jQuery(document).ready(function($) {
			$(".clickable-row").click(function() {
				window.location = $(this).data("href");
				// 				alert($(this).data("href"))
			});
		});
	</script>
</body>
</html>
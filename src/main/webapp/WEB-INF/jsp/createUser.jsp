<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>User List</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css"
	integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="/css/style.css">
</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<div class="container">
			<a class="navbar-brand" href="#">Desafio Eiti</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarNav" aria-controls="navbarNav"
				aria-expanded="false" aria-form:label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<li class="nav-item"><a class="nav-link" href="/usersList">List</a></li>
					<li class="nav-item"><a class="nav-link" href="/createUser">Create
							User</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<br>
	<div class="container container-fluid">
		<div class="row">
			<div class="col-md-12">
				<h1>User List</h1>

				<form:form method="POST" action="/saveUser" modelAttribute="user">
					<div class="row">
						<form:input type="hidden" class="form-control" id="id"
							aria-describedby="id" path="id"></form:input>
						<div class="col-md-6 form-group">
							<form:label for="username" path="username">Username</form:label>
							<form:input type="text" class="form-control" id="username"
								aria-describedby="username" placeholder="Enter Username"
								path="username"></form:input>
						</div>
						<div class="col-md-6 form-group">
							<form:label for="name" path="name">Name</form:label>
							<form:input type="text" class="form-control" id="name"
								aria-describedby="name" placeholder="Enter Name" path="name"></form:input>
						</div>
						<div class="col-md-6 form-group">
							<form:label for="surname" path="surname">Surname</form:label>
							<form:input type="text" class="form-control" id="surname"
								aria-describedby="surname" placeholder="Enter Surname"
								path="surname"></form:input>
						</div>
						<div class="col-md-6 form-group">
							<form:label for="email" path="email">Email</form:label>
							<form:input type="email" class="form-control" id="email"
								aria-describedby="email" placeholder="Enter Email" path="email"></form:input>
						</div>
						<div class="col-md-6 form-group">
							<form:label for="phone" path="phone">Phone</form:label>
							<form:input type="text" class="form-control" id="phone"
								aria-describedby="phone" placeholder="Enter Phone" path="phone"></form:input>
						</div>
						<div class="col-md-6 form-group">
							<form:label for="password" path="password">Password</form:label>
							<form:input type="password" class="form-control" id="password"
								aria-describedby="password" placeholder="Enter Password"
								path="password"></form:input>
						</div>
						<div class="col-md-12" style="text-align: right;">
							<a class="btn btn-secondary" href="/createUser" role="button">Clear
								Form</a>
							<button type="button" class="btn btn-danger" data-toggle="modal"
								data-target="#deleteUser">Delete</button>
							<input class="btn btn-primary" type="submit" value="Save">
						</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="deleteUser" tabindex="-1" role="dialog"
		aria-labelledby="deleteUser" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Delete User</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">You confirm user exclusion?</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal">Close</button>
					<a class="btn btn-danger" href="/deleteUser?id=${user.id}"
						role="button">Delete User</a>
				</div>
			</div>
		</div>
	</div>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
		integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js"
		integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em"
		crossorigin="anonymous"></script>
</body>
</html>